import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import bgswitchF5.BgSwitchF5;
import bgswitchF5.ConstantsEnc;
import bgswitchF5.F5Constants;
import bgswitchF5.requests.F5GetPoolRequest;
import bgswitchF5.requests.payload.F5PoolPayload;
import bgswitchF5.requests.payload.F5PoolPayload.Item;
import bgswitchF5.requests.payload.F5PoolPayload.Item_;

public class ScrapePoolConfig {
	public static void main(String[] args) {

		String ipAdd = ConstantsEnc.decrypt(F5Constants.ssHhostname);
		int upsCount = 0, totNodes = 0;
		Boolean poolFlag = false;
		Boolean isIpAddUp = false;
		Boolean goodToGo = true;
		String nodeSelfLink = "";

		// Setting up all the variables and objects
		BgSwitchF5 bg = BgSwitchF5.builder().username(ConstantsEnc.decrypt(F5Constants.Username))
				.password(ConstantsEnc.decrypt(F5Constants.Password)).build();

		try {
			bg.setup();

			// Logging with given credentials
			bg.login();

			F5PoolPayload poolResp = bg.sendRequest(new F5GetPoolRequest(""));

			try (FileWriter fw = new FileWriter("F5pool.csv", true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw)) {

				for (Item pools : poolResp.getItems()) {

					for (Item_ node : pools.getMembersReference().getItems()) {
						out.println(pools.getName() + "," + pools.getDescription() + "," + pools.getLoadBalancingMode()
								+ "," + pools.getFullPath() + "," + node.getName() + "," + node.getState() + ","
								+ node.getAddress() + "," + node.getFqdn().getAutopopulate() + "," + node.getMonitor());

						if (node.getAddress().equalsIgnoreCase(ipAdd)) {
							poolFlag = true;
							nodeSelfLink = node.getSelfLink();
							if (node.getState().equalsIgnoreCase("up")) {
								isIpAddUp = true;
							} else {
								isIpAddUp = false;
							}
							System.out.println(
									"IP Found -\n\tPool Name : " + pools.getName() + "\n\tNode Name : " + node.getName()
											+ "\n\tNode State : " + node.getState() + "\n\n\tAll Pool Members - ");
							for (Item_ node2 : pools.getMembersReference().getItems()) {
								System.out.println("\n\n\tNode Name : " + node2.getName() + "\n\tNode Ip : "
										+ node2.getAddress() + "\n\tNode State : " + node2.getState());

								// Get number of Nodes in the pool
								totNodes = pools.getMembersReference().getItems().size();

								if (node2.getState().equalsIgnoreCase("up")) {
									upsCount++;
								}
							}
							break;
						}
					}
					if (poolFlag) {
						break;
					}
				}

				while (poolResp.getNextLink() != null && !poolFlag) {
					poolResp = bg.sendRequest(new F5GetPoolRequest(poolResp.getNextLink()));
					for (Item pools : poolResp.getItems()) {

						for (Item_ node : pools.getMembersReference().getItems()) {

							out.println(pools.getName() + "," + pools.getDescription() + ","
									+ pools.getLoadBalancingMode() + "," + pools.getFullPath() + "," + node.getName()
									+ "," + node.getState() + "," + node.getAddress() + ","
									+ node.getFqdn().getAutopopulate() + "," + node.getMonitor());

							if (node.getAddress().equalsIgnoreCase(ipAdd)) {
								poolFlag = true;
								nodeSelfLink = node.getSelfLink();
								if (node.getState().equalsIgnoreCase("up")) {
									isIpAddUp = true;
								} else {
									isIpAddUp = false;
								}
								System.out.println("IP Found -\n\tPool Name : " + pools.getName() + "\n\tNode Name : "
										+ node.getName() + "\n\tNode State : " + node.getState()
										+ "\n\n\tAll Pool Members - ");
								for (Item_ node2 : pools.getMembersReference().getItems()) {
									System.out.println("\n\n\tNode Name : " + node2.getName() + "\n\tNode Ip : "
											+ node2.getAddress() + "\n\tNode State : " + node2.getState());

									// Get number of Nodes in the pool
									totNodes = pools.getMembersReference().getItems().size();

									if (node2.getState().equalsIgnoreCase("up")) {
										upsCount++;
									}
								}
								break;
							}
						}
						if (poolFlag) {
							break;
						}
					}
				}
				out.close();
			} catch (IOException e) {
				// exception handling left as an exercise for the reader
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
