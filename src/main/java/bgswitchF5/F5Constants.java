package bgswitchF5;

import lombok.Setter;
import lombok.ToString;

@ToString
public class F5Constants {

	// SSH Commands
	/*public static final String SSH_CMD_SHUTDOWN = "shutdown -r now";
	public static final String SSH_CMD_TOMCAT_RUNNING = "service tomcat8 status | grep -c \"active (running)\"";
	public static final String SSH_CMD_WEBAPPS_COUNT = "ls -lR /var/lib/tomcat8/webapps/*.war | wc -l";
	public static final String SSH_CMD_CATALINA_STARTED = "tac /var/log/tomcat8/catalina.out | grep 'INFO: Server startup in' -m 1 -B 2000000 | tac | wc -l";
	public static final String SSH_CMD_WAR_DEPLOYED_COUNT = "tac /var/log/tomcat8/catalina.out | grep 'Starting service Catalina$' -m 1 -B 2000000 | tac | grep -o -i \".war has finished\" | wc -l";

	public static final String API_URL = "https://zddlssf501.dlss.com/";

	public static final String blueiRule = "when HTTP_REQUEST {\r\n" + "pool BG-Blue-Pool\r\n" + "}";

	public static final String greeniRule = "when HTTP_REQUEST {\r\n" + "pool BG-Green-Pool\r\n" + "}";

	public static final String iRule = "bgSwitchIRule";*/

	public static final String SSH_CMD_SHUTDOWN = "shutdown -r now";
	public static final String SSH_CMD_TOMCAT_RUNNING = "systemctl status tomcat9 | grep -c \"active (running)\"";
	public static final String SSH_CMD_WEBAPPS_COUNT = "ls -lR /opt/tomcat9/webapps/*.war | wc -l";
	public static final String SSH_CMD_CATALINA_STARTED = "tac /opt/tomcat9/logs/catalina.out | grep 'Starting service \\[Catalina\\]' -m 1 -B 2000000 | tac | grep -o -i 'Server startup in' | wc -l";
	public static final String SSH_CMD_WAR_DEPLOYED_COUNT = "tac /opt/tomcat9/logs/catalina.out | grep 'Starting service \\[Catalina\\]' -m 1 -B 2000000 | tac | grep -o -i \".war\\] has finished\" | wc -l";

	public static final String API_URL = "https://zddlssf501.dlss.com/";

	public static final String blueiRule = "when HTTP_REQUEST {\r\n" + "pool BG-Blue-Pool\r\n" + "}";

	public static final String greeniRule = "when HTTP_REQUEST {\r\n" + "pool BG-Green-Pool\r\n" + "}";

	public static final String iRule = "bgSwitchIRule";
	
	@Setter
	public static String LOCALE = "en_US";

	/*
	public static final String Username = "CY0ZI9gRgoaBzMm+ZXjIdLdQRyFb78f+";
	public static final String Password = "XIC9kUKy3Hpjg508spl474IjMn+bFbJ9MQ4Ig+F8N2I=";
	public static final String ssHusername = "rQ/Rzl9KOauTOApePDnvWMrOD5/LWux2";
	public static final String ssHpassword = "PpZoJ9MEv41S1mCbcO9mo0qUrja0JIPW";
*/
	
	public static final String Username = "CY0ZI9gRgoaBzMm+ZXjIdLdQRyFb78f+";
	public static final String Password = "XIC9kUKy3Hpjg508spl474IjMn+bFbJ9MQ4Ig+F8N2I=";
	public static final String ssHusername = "uIz7DMnI51C8LIrK/KxWgg==";
	public static final String ssHpassword = "zYR62LwbO8NQYcNxafNa/opVk6uIlDR0JzdHRosreYGH0eFA0jBL5A==";
	public static final String ssHhostname = "10.241.36.11";
	public static final int ssHPort = 22;

}
