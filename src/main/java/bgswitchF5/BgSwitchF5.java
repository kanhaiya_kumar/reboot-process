package bgswitchF5;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import bgswitchF5.requests.F5LoginRequest;
import bgswitchF5.requests.F5Request;
import bgswitchF5.requests.payload.F5LoginPayload;
import bgswitchF5.requests.payload.F5LoginResult;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;

@Log4j
public class BgSwitchF5 {

	@Getter
	@Setter
	protected String username;

	@Getter
	@Setter
	protected String password;

	@Getter
	@Setter
	protected String loginProviderName;

	@Getter
	@Setter
	protected String authToken;

	@Getter
	@Setter
	protected String rankToken;

	@Getter
	protected boolean isLoggedIn;

	@Getter
	@Setter
	protected HttpResponse lastResponse;

	@Getter
	@Setter
	protected boolean debug;

	@Getter
	protected CloseableHttpClient client;

	/**
	 * @param username Username
	 * @param password Password
	 */
	@Builder
	public BgSwitchF5(String username, String password) {
		super();
		Logger.getRootLogger().setLevel(Level.ERROR);
		this.username = username;
		this.password = password;
		this.loginProviderName = "tmos";
	}

	/**
	 * Setup some variables
	 * 
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public void setup() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

		log.info("Setup...");

		if (StringUtils.isEmpty(this.username)) {
			throw new IllegalArgumentException("Username is mandatory.");
		}

		if (StringUtils.isEmpty(this.password)) {
			throw new IllegalArgumentException("Password is mandatory.");
		}

		// HttpClientBuilder builder = HttpClientBuilder.create();

		SSLContextBuilder builder = new SSLContextBuilder();
		builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
		SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(builder.build(),
				NoopHostnameVerifier.INSTANCE);
		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
				.register("http", new PlainConnectionSocketFactory()).register("https", sslConnectionSocketFactory)
				.build();

		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
		cm.setMaxTotal(100);
		this.client = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).setConnectionManager(cm)
				.build();

		// this.client = builder.build();
	}

	/**
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public F5LoginResult login() throws ClientProtocolException, IOException {

		log.info("Logging with user " + username + " and password " + password.replaceAll("[a-zA-Z0-9]", "*"));

		F5LoginPayload loginRequest = F5LoginPayload.builder().username(username).password(password)
				.loginProviderName(loginProviderName).build();
		F5LoginRequest req = new F5LoginRequest(loginRequest);
		F5LoginResult loginResult = this.sendRequest(req);
		if (loginResult.getToken() != null) {
			this.authToken = loginResult.getToken().getToken();
		}
		return loginResult;
	}

	/**
	 * Send request to endpoint
	 * 
	 * @param request Request object
	 * @return success flag
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public <T> T sendRequest(F5Request<T> request) throws ClientProtocolException, IOException {

		log.info("Sending request: " + request.getClass().getName());

		if (!this.isLoggedIn && request.requiresLogin()) {
			throw new IllegalStateException("Need to login first!");
		}

		// wait to simulate real human interaction
		randomWait();

		request.setApi(this);
		T response = request.execute();

		log.debug("Result for " + request.getClass().getName() + ": " + response);

		return response;
	}

	@SneakyThrows
	private void randomWait() {
		Thread.sleep(200);
	}
}