package bgswitchF5.requests;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

import bgswitchF5.F5Constants;
import lombok.extern.log4j.Log4j;

@Log4j
public abstract class F5GetRequest<T> extends F5Request<T> {

	@Override
	public String getMethod() {
		return "GET";
	}

	@Override
	public T execute() throws ClientProtocolException, IOException {

		HttpGet post = new HttpGet(F5Constants.API_URL + getUrl());
		post.addHeader("Content-Type", "application/json");
		post.addHeader("Accept-Language", "en-US");
		post.addHeader("X-F5-Auth-Token", api.getAuthToken());

		HttpResponse response = api.getClient().execute(post);
		api.setLastResponse(response);

		int resultCode = response.getStatusLine().getStatusCode();
		String content = EntityUtils.toString(response.getEntity());
		log.debug("Response : " + content);
		post.releaseConnection();

		return parseResult(resultCode, content);
	}

}