package bgswitchF5.requests;

import com.fasterxml.jackson.databind.ObjectMapper;

import bgswitchF5.requests.payload.F5LoginPayload;
import bgswitchF5.requests.payload.F5LoginResult;
import bgswitchF5.requests.payload.F5NodePayload;
import bgswitchF5.requests.payload.F5NodeResult;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;

@AllArgsConstructor
@Log4j
public class F5EnableNodeRequest extends F5PutRequest<F5NodeResult> {

	private F5NodePayload payload;
	private String selfLink;

	@Override
	public String getUrl() {
		return selfLink.replace("https://localhost", "");
	}

	@Override
	@SneakyThrows
	public String getPayload() {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(payload);
	}

	@Override
	@SneakyThrows
	public F5NodeResult parseResult(int statusCode, String content) {
		return parseJson(statusCode, content, F5NodeResult.class);
	}

	@Override
	public boolean requiresLogin() {
		return false;
	}

}