package bgswitchF5.requests.payload;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
public class F5LoginResult extends StatusResult {

	public String username;
	public LoginReference loginReference;
	public String loginProviderName;
	public Token token;
	public Long generation;
	public Long lastUpdateMicros;

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class LoginReference {

		public String link;

	}

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class Token {

		public String token;
		public String name;
		public String userName;
		public String authProviderName;
		public User user;
		public List<GroupReference> groupReferences = null;
		public Long timeout;
		public String startTime;
		public String address;
		public String partition;
		public Long generation;
		public Long lastUpdateMicros;
		public Long expirationMicros;
		public String kind;
		public String selfLink;

	}

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class User {

		public String link;

	}

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class GroupReference {

		public String link;

	}
}