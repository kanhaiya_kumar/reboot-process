package bgswitchF5.requests.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Status Result
 * @author Bruno Candido Volpato da Cunha
 *
 */
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@NoArgsConstructor
public class StatusResult {
    @NonNull
    private int code;
    private String message;
    private String originalRequestBody;
    private String referer;
    private Long restOperationId;
    private String kind;
    
}