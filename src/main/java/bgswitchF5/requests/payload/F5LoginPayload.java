package bgswitchF5.requests.payload;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class F5LoginPayload  {
    private String username;
    private String password;
    private String loginProviderName;

}