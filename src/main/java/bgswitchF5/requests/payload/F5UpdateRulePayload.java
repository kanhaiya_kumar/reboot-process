package bgswitchF5.requests.payload;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class F5UpdateRulePayload {
    private String apiAnonymous;

}