package bgswitchF5.requests.payload;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
public class F5NodeResult extends StatusResult {

	public String kind;
	public String name;
	public String partition;
	public String fullPath;
	public Integer generation;
	public String selfLink;
	public String address;
	public Integer connectionLimit;
	public Integer dynamicRatio;
	public String ephemeral;
	public Fqdn fqdn;
	public String inheritProfile;
	public String logging;
	public String monitor;
	public Integer priorityGroup;
	public String rateLimit;
	public Integer ratio;
	public String session;
	public String state;

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class Fqdn {

		public String autopopulate;

	}

}