package bgswitchF5.requests.payload;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
public class F5UpdateRuleResult  extends StatusResult{

	public String kind;
	public String name;
	public String fullPath;
	public Long generation;
	public String selfLink;
	public String apiAnonymous;

}