package bgswitchF5.requests.payload;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
public class F5PoolPayload  extends StatusResult {

	public String kind;
	public String selfLink;
	public Integer currentItemCount;
	public Integer itemsPerPage;
	public Integer pageIndex;
	public Integer startIndex;
	public Integer totalItems;
	public Integer totalPages;
	public List<Item> items = null;
	public String nextLink;

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class AppServiceReference {

		public String link;

	}

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor

	public static class AppServiceReference_ {

		public String link;

	}

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class Fqdn {

		public String autopopulate;

	}

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class Item {

		public String kind;
		public String name;
		public String partition;
		public String fullPath;
		public Integer generation;
		public String selfLink;
		public String allowNat;
		public String allowSnat;
		public String ignorePersistedWeight;
		public String ipTosToClient;
		public String ipTosToServer;
		public String linkQosToClient;
		public String linkQosToServer;
		public String loadBalancingMode;
		public Integer minActiveMembers;
		public Integer minUpMembers;
		public String minUpMembersAction;
		public String minUpMembersChecking;
		public String monitor;
		public Integer queueDepthLimit;
		public String queueOnConnectionLimit;
		public Integer queueTimeLimit;
		public Integer reselectTries;
		public String serviceDownAction;
		public Integer slowRampTime;
		public MembersReference membersReference;
		public String subPath;
		public String appService;
		public AppServiceReference_ appServiceReference;
		public String description;

	}

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class Item_ {

		public String kind;
		public String name;
		public String partition;
		public String fullPath;
		public Integer generation;
		public String selfLink;
		public String address;
		public Integer connectionLimit;
		public Integer dynamicRatio;
		public String ephemeral;
		public Fqdn fqdn;
		public String inheritProfile;
		public String logging;
		public String monitor;
		public Integer priorityGroup;
		public String rateLimit;
		public Integer ratio;
		public String session;
		public String state;
		public NameReference nameReference;
		public String appService;
		public AppServiceReference appServiceReference;

	}

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor

	public static class MembersReference {

		public String link;
		public Boolean isSubcollection;
		public List<Item_> items = null;

	}

	@Getter
	@Setter
	@ToString(callSuper = true)
	@NoArgsConstructor
	public static class NameReference {

		public String link;

	}

}