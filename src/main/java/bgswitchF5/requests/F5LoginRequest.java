package bgswitchF5.requests;

import com.fasterxml.jackson.databind.ObjectMapper;

import bgswitchF5.requests.payload.F5LoginPayload;
import bgswitchF5.requests.payload.F5LoginResult;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;

@AllArgsConstructor
@Log4j
public class F5LoginRequest extends F5PostRequest<F5LoginResult> {

    private F5LoginPayload payload;

    @Override
    public String getUrl() {
        return "mgmt/shared/authn/login";
    }

    @Override
    @SneakyThrows
    public String getPayload() {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(payload);
    }

    @Override
    @SneakyThrows
    public F5LoginResult parseResult(int statusCode, String content) {
        return parseJson(statusCode, content, F5LoginResult.class);
    }

    @Override
    public boolean requiresLogin() {
        return false;
    }

}