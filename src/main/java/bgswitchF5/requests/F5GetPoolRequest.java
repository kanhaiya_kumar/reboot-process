package bgswitchF5.requests;

import bgswitchF5.requests.payload.F5PoolPayload;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;

@AllArgsConstructor
@Log4j
public class F5GetPoolRequest extends F5GetRequest<F5PoolPayload> {

	private String nextLink;

	@Override
	public String getUrl() {
		if (nextLink != null && !nextLink.isEmpty()) {
			// "nextLink":
			// "https://localhost/mgmt/tm/ltm/pool?$top=4&expandSubcollections=true&$skip=4&ver=12.1.3.3"
			return nextLink.replace("https://localhost", "");
		} else {
			return "/mgmt/tm/ltm/pool/?expandSubcollections=true&$top=4";
		}

	}

	@Override
	@SneakyThrows
	public String getPayload() {
		return "";
	}

	@Override
	@SneakyThrows
	public F5PoolPayload parseResult(int statusCode, String content) {
		return parseJson(statusCode, content, F5PoolPayload.class);
	}

	@Override
	public boolean requiresLogin() {
		return false;
	}

}