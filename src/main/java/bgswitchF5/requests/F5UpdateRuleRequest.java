package bgswitchF5.requests;

import com.fasterxml.jackson.databind.ObjectMapper;

import bgswitchF5.requests.payload.F5LoginPayload;
import bgswitchF5.requests.payload.F5UpdateRulePayload;
import bgswitchF5.requests.payload.F5UpdateRuleResult;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;

@AllArgsConstructor
@Log4j
public class F5UpdateRuleRequest extends F5PatchRequest<F5UpdateRuleResult> {

    private F5UpdateRulePayload payload;
    private String iRuleName;

    @Override
    public String getUrl() {
        return "/mgmt/tm/ltm/rule/" + iRuleName;
    }

    @Override
    @SneakyThrows
    public String getPayload() {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(payload);
    }

    @Override
    @SneakyThrows
    public F5UpdateRuleResult parseResult(int statusCode, String content) {
        return parseJson(statusCode, content, F5UpdateRuleResult.class);
    }

    @Override
    public boolean requiresLogin() {
        return false;
    }

}