package bgswitchF5.requests;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import bgswitchF5.F5Constants;
import lombok.extern.log4j.Log4j;


@Log4j
public abstract class F5PostRequest<T> extends F5Request<T> {

    @Override
    public String getMethod() {
        return "POST";
    }
    
    @Override
    public T execute() throws ClientProtocolException, IOException {
    	
        HttpPost post = new HttpPost(F5Constants.API_URL + getUrl());
        post.addHeader("Content-Type", "application/json");
        post.addHeader("Accept-Language", "en-US");
       
        String payload = getPayload();
        log.debug("Base Payload: " + payload);
        
        post.setEntity(new StringEntity(payload));
        
        HttpResponse response = api.getClient().execute(post);
        api.setLastResponse(response);
        
        int resultCode = response.getStatusLine().getStatusCode();
        String content = EntityUtils.toString(response.getEntity());
      //  System.out.println(content);
        log.debug("Response : " + content);
        post.releaseConnection();

        return parseResult(resultCode, content);
    }

}