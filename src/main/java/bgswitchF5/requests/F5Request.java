package bgswitchF5.requests;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.brunocvcunha.inutils4j.MyStreamUtils;

import bgswitchF5.BgSwitchF5;
import bgswitchF5.requests.payload.StatusResult;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;

@AllArgsConstructor
@NoArgsConstructor
@Log4j
public abstract class F5Request<T> {

	@Getter
	@Setter
	protected BgSwitchF5 api;

	/**
	 * @return the url
	 */
	public abstract String getUrl();

	/**
	 * @return the method
	 */
	public abstract String getMethod();

	/**
	 * @return the payload
	 */
	public String getPayload() {
		return null;
	}

	/**
	 * @return the result
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public abstract T execute() throws ClientProtocolException, IOException;

	/**
	 * Process response
	 * 
	 * @param resultCode Status Code
	 * @param content    Content
	 */
	public abstract T parseResult(int resultCode, String content);

	/**
	 * @return if request must be logged in
	 */
	public boolean requiresLogin() {
		return true;
	}

	/**
	 * Parses Json into type, considering the status code
	 * 
	 * @param statusCode HTTP Status Code
	 * @param str        Entity content
	 * @param clazz      Class
	 * @return Result
	 */
	@SneakyThrows
	public <U> U parseJson(int statusCode, String str, Class<U> clazz) {

		if (clazz.isAssignableFrom(StatusResult.class)) {

			// TODO: implement a better way to handle exceptions
			if (statusCode == HttpStatus.SC_NOT_FOUND) {
				StatusResult result = (StatusResult) clazz.newInstance();
				result.setCode(401);
				result.setMessage("SC_NOT_FOUND");
				return (U) result;
			} else if (statusCode == HttpStatus.SC_FORBIDDEN) {
				StatusResult result = (StatusResult) clazz.newInstance();
				result.setCode(401);
				result.setMessage("SC_FORBIDDEN");
				return (U) result;
			}
		}

		return parseJson(str, clazz);
	}

	/**
	 * Parses Json into type
	 * 
	 * @param str   Entity content
	 * @param clazz Class
	 * @return Result
	 */
	@SneakyThrows
	public <U> U parseJson(String str, Class<U> clazz) {

		/*
		 * if (log.isInfoEnabled()) {
		 * 
		 * if (log.isDebugEnabled()) { log.debug("Reading " + clazz.getSimpleName() +
		 * " from " + str); } else { String printStr = str; if (printStr.length() > 128)
		 * { printStr = printStr.substring(0, 128); } log.info("Reading " +
		 * clazz.getSimpleName() + " from " + printStr); }
		 * 
		 * }
		 */

		ObjectMapper objectMapper = new ObjectMapper()
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
				.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

		return objectMapper.readValue(str, clazz);
	}

	/**
	 * Parses Json into type
	 * 
	 * @param is    Entity stream
	 * @param clazz Class
	 * @return Result
	 */
	@SneakyThrows
	public T parseJson(InputStream is, Class<T> clazz) {
		return this.parseJson(MyStreamUtils.readContent(is), clazz);
	}

	/**
	 * @return payload should be signed
	 */
	public boolean isSigned() {
		return true;
	}

}