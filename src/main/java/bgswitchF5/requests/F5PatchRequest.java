package bgswitchF5.requests;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import bgswitchF5.F5Constants;
import lombok.extern.log4j.Log4j;


@Log4j
public abstract class F5PatchRequest<T> extends F5Request<T> {

    @Override
    public String getMethod() {
        return "PATCH";
    }
    
    @Override
    public T execute() throws ClientProtocolException, IOException {
    	
        HttpPatch post = new HttpPatch(F5Constants.API_URL + getUrl());
        post.addHeader("Content-Type", "application/json");
        post.addHeader("X-F5-Auth-Token", api.getAuthToken());
      
        String payload = getPayload();
        log.debug("Base Payload: " + payload);
        
        post.setEntity(new StringEntity(payload));
        
        HttpResponse response = api.getClient().execute(post);
        api.setLastResponse(response);
        
        int resultCode = response.getStatusLine().getStatusCode();
        String content = EntityUtils.toString(response.getEntity());
        log.debug("Response : " + content);
        post.releaseConnection();

        return parseResult(resultCode, content);
    }

}