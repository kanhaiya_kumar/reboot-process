package bgswitchF5;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;

public class ConstantsEnc {

	public static PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();

	public static void init() {

		if (!encryptor.isInitialized()) {
			SimpleStringPBEConfig config = new SimpleStringPBEConfig();
			config.setPassword("ebead908c52a477a91b3e27ae2161cb168eb5884");
			config.setAlgorithm("PBEWithMD5AndTripleDES");
			config.setPoolSize("1");
			config.setKeyObtentionIterations("1000");
			config.setProviderName("SunJCE");
			config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
			config.setStringOutputType("base64");
			encryptor.setConfig(config);
		}
	}

	public static String encrypt(String plaintext) {
		init();
		return encryptor.encrypt(plaintext);

	}

	public static String decrypt(String encryptedtext) {
		init();
		return encryptor.decrypt(encryptedtext);

	}
}
