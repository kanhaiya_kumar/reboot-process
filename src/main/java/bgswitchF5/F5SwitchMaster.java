package bgswitchF5;

//import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import bgswitchF5.BgSwitchF5;
import bgswitchF5.ConstantsEnc;
import bgswitchF5.F5Constants;
import bgswitchF5.requests.F5EnableNodeRequest;
import bgswitchF5.requests.F5GetPoolRequest;
import bgswitchF5.requests.payload.F5LoginResult;
import bgswitchF5.requests.payload.F5NodePayload;
import bgswitchF5.requests.payload.F5NodeResult;
import bgswitchF5.requests.payload.F5PoolPayload;
import bgswitchF5.requests.payload.F5PoolPayload.Item;
import bgswitchF5.requests.payload.F5PoolPayload.Item_;
import ssh.SSHConnectionManager;
import java.time.Duration;
import java.time.Instant;

public class F5SwitchMaster {

	//String ipAdd = F5Constants.ssHhostname;
	String ipAdd = "10.41.98.128";
	int upsCount = 0, totNodes = 0;
	Boolean poolFlag = false;
	Boolean isIpAddUp = false;
	Boolean goodToGo = true;
	String nodeSelfLink = "";
	F5NodePayload node;
	F5NodeResult enableResp;

	public static void main(String[] args) {
		F5SwitchMaster a = new F5SwitchMaster();
		/*
		 * SSHConnectionManager ssh1 = new SSHConnectionManager(); try { ssh1.open();
		 * ssh1.runCommand("nohup /opt/tomcat/bin/startup.sh").replace("\n",
		 * "").replace("\r", ""); } catch (JSchException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * 
		 * 
		 */
		a.F5();
	}

	public void F5() {

		// Setting up all the variables and objects
		BgSwitchF5 bg = BgSwitchF5.builder().username(ConstantsEnc.decrypt(F5Constants.Username))
				.password(ConstantsEnc.decrypt(F5Constants.Password)).build();

		try {

			log("Setting F5 API configuration.");
			bg.setup();

			log("Login to F5 with user " + ConstantsEnc.decrypt(F5Constants.Username));
			// Logging with given credentials
			F5LoginResult loginRes = bg.login();
			if (loginRes.getToken() != null) {
				log("Login Success.");
			} else {
				log("Error in Login with user id " + ConstantsEnc.decrypt(F5Constants.Username) + " Reason : "
						+ loginRes.getMessage());
				return;
			}

			System.out.println("\n");
			int pageCount = 0;

			log("Searching for IP " + ipAdd + " in F5 Pool List " + ++pageCount);
			// Fetch pool details from first page
			F5PoolPayload poolResp = bg.sendRequest(new F5GetPoolRequest(""));
			scrapeF5Page(poolResp);

			// Fetch pool details from all other pages until the last page is reached
			while (poolResp.getNextLink() != null && !poolFlag) {
				log("Searching for IP " + ipAdd + " in F5 Pool List " + ++pageCount);
				poolResp = bg.sendRequest(new F5GetPoolRequest(poolResp.getNextLink()));
				scrapeF5Page(poolResp);
			}
			System.out.println("\n");
			// If Node found then Check risk
			if (poolFlag) { // IF pool member is present

				log("Checking the current status of all pool members.");
				if (checkRisk()) {
					// Disable node if everything is good
					System.out.println("\n");
					if (disableNodeinPool(bg)) {
						System.out.println("\n");
						executedOverSsh(bg);
					}

				}
			} else {
				System.out.println("\n");
				log("The Node with IP " + ipAdd + " is not present in any pool.");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	public void executedOverSsh(BgSwitchF5 bg) {

		String webAppsCnt = "";
		String ret = "";

		SSHConnectionManager ssh1 = new SSHConnectionManager();

		try {
			Instant start = Instant.now();

			ssh1.open();
			ret = ssh1.runCommand(F5Constants.SSH_CMD_SHUTDOWN);
			log("Node with IP " + ipAdd + " Was Restarted.");
			ssh1.close();

			log("Waiting for 60 Seconds.");
			Thread.sleep(40000); // Wait a Minute

			ssh1.open();

			webAppsCnt = ssh1.runCommand(F5Constants.SSH_CMD_WEBAPPS_COUNT).replace("\n", "").replace("\r", "");
			log("Total War files to Deploy : " + webAppsCnt);

			log("Checking Tomcat service status.");
			Thread.sleep(5000);

			// Tomcat is Running
			int started = Integer
					.parseInt(ssh1.runCommand(F5Constants.SSH_CMD_TOMCAT_RUNNING).replace("\n", "").replace("\r", ""));

			if (started > 0) {
				log("Tomcat service Started.");

				while (true) {

					ret = ssh1.runCommand(F5Constants.SSH_CMD_CATALINA_STARTED).replace("\n", "").replace("\r", "");

					// All is deployed
					if (ret.equalsIgnoreCase("1")) {

						log("All Packages are Deployed.");

						// Enable the Node in F5 Again
						System.out.println("\n");
						enableNodeinPool(bg);
						
						Instant end = Instant.now();
						Duration timeElapsed = Duration.between(start, end);
						System.out.println("\n");
						log("Task Completed! Total Downtime : " + timeElapsed.toMinutes() + " Minutes.");
						break;

					}
					// Validate number of deployed war file
					else {

						int currDeployed = Integer.parseInt(ssh1.runCommand(F5Constants.SSH_CMD_WAR_DEPLOYED_COUNT)
								.replace("\n", "").replace("\r", ""));
						log("Total War files Deployed : " + currDeployed + "/" + webAppsCnt);
						Thread.sleep(25000);

					}
				}

			}
			// Tomcat is not Running
			else {

				log("Unable to Start Tomcat.");

			}

			ssh1.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void log(String log) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss ");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now) + " : " + log);
	}

	public void scrapeF5Page(F5PoolPayload poolResp) {

		for (Item pools : poolResp.getItems()) {

			for (Item_ node : pools.getMembersReference().getItems()) {
				if (node.getAddress().equalsIgnoreCase(ipAdd)) {
					poolFlag = true;
					nodeSelfLink = node.getSelfLink();
					if (node.getState().equalsIgnoreCase("up")) {
						isIpAddUp = true;
					} else {
						isIpAddUp = false;
					}

					System.out.println("\nAll Pool Members :- \n\tNode Name\tNode IP\t\tNode Session\tNode State");
					for (Item_ node2 : pools.getMembersReference().getItems()) {
						System.out.println("\t" + node2.getName() + "\t" + node2.getAddress() + "\t"
								+ node2.getSession() + "\t" + node2.getState());

						// Get number of Nodes in the pool
						totNodes = pools.getMembersReference().getItems().size();

						// "session":"monitor-enabled","state":"up"
						// session=user-disabled, state=user-down
						// session=user-enabled, state=down)
						if ((node2.getSession().equalsIgnoreCase("user-enabled")
								|| (node2.getSession().equalsIgnoreCase("monitor-enabled"))
										&& node2.getState().equalsIgnoreCase("up"))) {
							upsCount++;
						}
					}

					System.out.println("\nIP Found -\n\tPool Name : " + pools.getName() + "\n\tNode Name : "
							+ node.getName() + "\n\tNode Session : " + node.getSession() + "\n\tNode State : "
							+ node.getState());

					break;
				}
			}
			if (poolFlag) {
				break;
			}
		}
	}

	public boolean checkRisk() {

		goodToGo = false;

		// Condition for moving further
		if (totNodes == 1 && upsCount < 1) {
			goodToGo = true; // If there is single node and which is offline then go ahead
			log("\tOnly one node present in the Pool with status DOWN or UnKnown.\n\tThe Process will continue.");
		} else if (totNodes == 1 && upsCount == 1) {
			goodToGo = false;
			log("\tOnly one node present in the Pool with status UP.\n\tStopping the process.");
		} else if (totNodes == 2 && upsCount == 1) {
			if (isIpAddUp) {
				goodToGo = false;
				log("\tOnly Two nodes present in the Pool with Target Node only UP.\n\tStopping the process.");
			} else {
				goodToGo = true;
				log("\tOnly Two node present in the Pool with Target DOWN or UnKnown.\n\tThe Process will continue.");
			}
		} else {
			if (totNodes > 2) {
				if (upsCount <= 1 && isIpAddUp) {
					goodToGo = false;
					log("\tOne or less than one node is Acitve out of " + totNodes
							+ " in the Pool with Target Node only UP.\n\tStopping the process.");
				} else {
					goodToGo = true;
					log("\t" + totNodes + " present in the Pool.\n\tThe Process will continue.");
				}
			} else {
				goodToGo = true;
				log("\tThe Process will continue.");

			}

		}

		return goodToGo;
	}

	public boolean enableNodeinPool(BgSwitchF5 bg) throws ClientProtocolException, IOException, InterruptedException {

		log("Login to F5 again with user " + ConstantsEnc.decrypt(F5Constants.Username));
		// Logging with given credentials
		F5LoginResult loginRes = bg.login();
		if (loginRes.getToken() != null) {
			log("Login Success.");
		} else {
			log("Error in Login with user id " + ConstantsEnc.decrypt(F5Constants.Username) + " Reason : "
					+ loginRes.getMessage());
			return false;
		}

		node = F5NodePayload.builder().session("user-enabled").state("user-up").build();
		enableResp = bg.sendRequest(new F5EnableNodeRequest(node, nodeSelfLink));
		if (enableResp.getSession() != null) {
			if (enableResp.getSession().equalsIgnoreCase("user-enabled")) {
				log("Node was enabled in F5 Pool.");
			} else {
				log("Failed to enabled node in F5 Pool." + enableResp.toString());
				return false;
			}
		} else {
			log("Failed to enabled node in F5 Pool." + enableResp.toString());
			return false;
		}

		Thread.sleep(1000);
		return true;
	}

	// The node for given SelfLink will be disabled
	public boolean disableNodeinPool(BgSwitchF5 bg) throws ClientProtocolException, IOException, InterruptedException {

		node = F5NodePayload.builder().session("user-disabled").state("user-down").build();
		enableResp = bg.sendRequest(new F5EnableNodeRequest(node, nodeSelfLink));
		if (enableResp.getSession() != null) {
			if (enableResp.getSession().equalsIgnoreCase("user-disabled")) {
				log("Node was disabled in F5 Pool.");
			} else {
				log("Failed to disable node in F5 Pool." + enableResp.toString());
				return false;
			}
		} else {
			log("Failed to disable node in F5 Pool." + enableResp.toString());
			return false;
		}
		Thread.sleep(1000);
		return true;
	}
}
