package ssh;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import bgswitchF5.ConstantsEnc;
import bgswitchF5.F5Constants;

public class SSHConnectionManager {

	private Session session;

	private String username = ConstantsEnc.decrypt(F5Constants.ssHusername);
	private String password = ConstantsEnc.decrypt(F5Constants.ssHpassword);
	private String hostname = F5Constants.ssHhostname;
	private int ssHPort  = F5Constants.ssHPort;

	public SSHConnectionManager() {
	}

	public SSHConnectionManager(String hostname, String username, String password, int ssHPort) {
		this.hostname = hostname;
		this.username = username;
		this.password = password;
		this.ssHPort = ssHPort;
	}

	public void open() throws JSchException {
		open(this.hostname, this.username, this.password, this.ssHPort);
	}

	public void open(String hostname, String username, String password, int ssHPort) throws JSchException {

		JSch jSch = new JSch();
		
		//System.out.println(username+"+"+password);
		
		session = jSch.getSession(username, hostname, ssHPort);
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no"); // not recommended
		session.setConfig(config);
		session.setPassword(password);

		log("Connecting SSH to " + hostname );
		session.connect();
		
		log("Connected!");
	}

	public void log(String log) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss ");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now) + " : " + log);
	}
	
	public String runCommand(String command) throws JSchException, IOException {

		String ret = "";

		if (!session.isConnected())
			throw new RuntimeException("Not connected to an open session.  Call open() first!");

		ChannelExec channel = null;
		channel = (ChannelExec) session.openChannel("exec");

		channel.setCommand("sudo -S -p '' " + command);
		channel.setInputStream(null);

		PrintStream out = new PrintStream(channel.getOutputStream());
		((ChannelExec) channel).setErrStream(System.err);
		InputStream in = channel.getInputStream(); // channel.getInputStream();
		((ChannelExec) channel).setPty(true);
		channel.connect();
		out.write((password + "\n").getBytes());
		out.flush();

		// you can also send input to your running process like so:
		// String someInputToProcess = "something";
		// out.println(someInputToProcess);
		// out.flush();

		ret = getChannelOutput(channel, in);

		channel.disconnect();

	//	System.out.println("Command Executed : " + command);

		return ret;
	}

	private String getChannelOutput(Channel channel, InputStream in) throws IOException {

		byte[] buffer = new byte[1024];
		StringBuilder strBuilder = new StringBuilder();

		String line = "";
		while (true) {
			while (in.available() > 0) {
				int i = in.read(buffer, 0, 1024);
				if (i < 0) {
					break;
				}

				line = new String(buffer, 0, i);
				strBuilder.append(line);
				//System.out.println(line);
			}

			if (line.contains("logout")) {
				break;
			}

			if (channel.isClosed()) {
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ee) {
			}
		}

		return strBuilder.toString();
	}

	public void close() {
		session.disconnect();
		log("Disconnected channel and session");
	}

	public static void main(String[] args) {

//		int webAppsCnt = 0, statusCnt = 0;
//
//		SSHConnectionManager ssh = new SSHConnectionManager();
//		try {
//
//			ssh.open();
//			String ret = ssh.runCommand("shutdown -r now");
//			ssh.close();
//
//			Thread.sleep(30000);
//
//			ssh.open();
//			ret = ssh.runCommand("service tomcat8 status | grep -c \"active (running)\"");
//			if (ret.equalsIgnoreCase("1")) {
//				// Tomcat is Running
//				webAppsCnt = Integer.parseInt(ssh.runCommand("ls -lR /var/lib/tomcat8/webapps/*.war | wc -l"));
//				System.out.println("\n\nTotal War files : " + webAppsCnt);
//
//				Thread.sleep(5000);
//				
//				while (true) {
//					int started = Integer.parseInt(ssh.runCommand(
//							"tac /var/log/tomcat8/catalina.out | grep 'INFO: Server startup in' -m 1 -B 2000000 | tac | wc -l"));
//					if (started > 0) {
//						// All is deployed
//						break;
//					} else {
//						int currDeployed = Integer.parseInt(ssh.runCommand(
//								"tac /var/log/tomcat8/catalina.out | grep 'Starting service Catalina$' -m 1 -B 2000000 | tac | grep -o -i \".war has finished\" | wc -l"));
//						System.out.println("\n\nTotal War files Deployed : " + currDeployed + " \\ " + webAppsCnt);
//						Thread.sleep(5000);
//					}
//				}
//
//			} else {
//				System.out.println("\n\n\nTomcat is not yet Running\n\n\n");
//			}
////			String ret = ssh.runCommand("shutdown -r now");
////			// String ret = ssh.runCommand("sh /opt/tomcat/bin/catalina.sh start");
////			System.out.println(ret);
////			// ssh.close();
////
////			Thread.sleep(19000);
////
////			ssh.open();
////			// String ret = ssh.runCommand("shutdown -r now");
////			ret = ssh.runCommand("sh /opt/tomcat/bin/startup.sh");
////			System.out.println(ret);
////			// ssh.close();
////
////			Thread.sleep(19000);
////			// ssh.open();
////			ret = ssh.runCommand("ps -ef | grep tomcat");
////
////			System.out.println(ret);
////
////			ret = ssh.runCommand("tail -f /opt/tomcat/logs/catalina.out");
////
////			System.out.println(ret);
//
//			ssh.close();
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}

/*
 * Count of war file in webapps: ls -lR /var/lib/tomcat8/webapps/*.war | wc -l
 * 
 * 
 * 
 * Validate wars process status in catalina:
 * 
 * tac /var/log/tomcat8/catalina.out | grep 'Starting service Catalina$' -m 1 -B
 * 2000000 | tac | grep -o -i ".war has finished" | wc -l grep -o -i
 * ".war has finished" /var/log/tomcat8/catalina.out | wc -l
 * 
 * 
 * 
 * Validate the completion of catelina file: tac /var/log/tomcat8/catalina.out |
 * grep 'INFO: Server startup in' -m 1 -B 2000000 | tac | wc -l
 */